package com.diego.builders.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.diego.builders.services.DBService;

@Configuration
@Profile("dev")
public class DevConfig {
	
	@Autowired
	private DBService dbService;

	@Value("{spring.jpa.hibernate.ddl-auto}")
	private String strategy;
	
	@Bean
	public boolean instantiateDatabase() {
	
		//caso a variavel do perfil de DEV não seja CREATE não fica recriando o banco ao iniciar a aplicação
		if(!"create".equals(strategy)){
			return false;
		}
		
		dbService.instantiateTestDataBase();
		
		return true;
	}
	
}
