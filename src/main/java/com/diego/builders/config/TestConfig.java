package com.diego.builders.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.diego.builders.services.DBService;

@Configuration
@Profile("test")
public class TestConfig {
	
	@Autowired
	private DBService dbService;

	@Bean
	public boolean instantiateDatabase() {
		
		//instancia sempre uma nova instancia das tabelas / dados do banco de dados
		dbService.instantiateTestDataBase();
		
		return true;
	}
	
}
