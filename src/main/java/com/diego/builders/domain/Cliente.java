package com.diego.builders.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import com.diego.builders.domain.enums.SexoEnum;
import com.diego.builders.services.validation.ClienteInsert;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@ClienteInsert
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class Cliente implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotEmpty(message = "Campo Nome Preenchimento Obrigatório.")
	private String nome;
	private Integer idade;
	@NotEmpty(message = "Campo Cpf Preenchimento Obrigatório.")
	@Column(unique = true)
	private String cpf;
	private SexoEnum sexo;

	public Cliente(Integer id, String nome, Integer idade, String cpf, SexoEnum sexo) {
		super();
		this.id = id;
		this.nome = nome;
		this.idade = idade;
		this.cpf = cpf;
		this.sexo = sexo;
	}

}
