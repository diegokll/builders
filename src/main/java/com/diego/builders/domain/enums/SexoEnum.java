package com.diego.builders.domain.enums;

import lombok.Getter;

@Getter
public enum SexoEnum {

	
	MASCULINO(0, "Masculino"),
	FEMININO(1, "Feminino");
	
	private int cod;
	private String descricao;
	
	private SexoEnum(int cod, String descricao){
		this.cod = cod;
		this.descricao = descricao;
	}

	public static SexoEnum toEnum(Integer cod) {
		if(cod==null) {
			return null;
		}
		
		for(SexoEnum x : SexoEnum.values()) {
			if(cod.equals(x.getCod())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id Inválido: "+cod);
	}
}
