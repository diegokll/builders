package com.diego.builders.repositories;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diego.builders.domain.Cliente;
import com.diego.builders.domain.enums.SexoEnum;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer>{

	@Transactional
	Optional<Cliente> findByCpf(String cpf);
	
	@Transactional
	Optional<Cliente> findByNome(String nome);
	
	@Transactional
	List<Cliente> findBySexo(SexoEnum sexo);
	
	@Query("SELECT obj FROM Cliente obj WHERE obj.nome LIKE %:nome%")
	Page<Cliente> findPageName(@Param("nome") String nome, Pageable pageNome);
	
	@Query("SELECT obj FROM Cliente obj WHERE obj.idade = :idade")
	Page<Cliente> findPageIdade(@Param("idade") Integer idade, Pageable pageNome);
	
	@Query("SELECT obj FROM Cliente obj WHERE obj.sexo = :sexo")
	Page<Cliente> findPageSexo(@Param("sexo") SexoEnum sexo, Pageable pageNome);
}
