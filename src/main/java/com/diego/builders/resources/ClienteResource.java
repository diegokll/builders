package com.diego.builders.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.diego.builders.domain.Cliente;
import com.diego.builders.domain.enums.SexoEnum;
import com.diego.builders.services.ClienteService;

@Validated
@RestController
@RequestMapping(value="/api/v1/clientes")
public class ClienteResource {
	
	@Autowired
	private ClienteService service;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> find(@PathVariable Integer id) {
		
		Cliente obj = service.find(id);
		return ResponseEntity.ok().body(obj);
		
	}
	
	@RequestMapping(value = "/cpf/{cpf}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> findByCpf(@PathVariable String cpf) {
		
		Cliente obj = service.findByCpf(cpf);
		return ResponseEntity.ok().body(obj);
		
	}
	
	@RequestMapping(value = "/nome/{nome}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> findByNome(@PathVariable String nome) {
		
		Cliente obj = service.findByNome(nome);
		return ResponseEntity.ok().body(obj);
		
	}
	
	@RequestMapping(value = "/sexo/{sexo}", method = RequestMethod.GET)
	public ResponseEntity<List<Cliente>> findBySexo(@PathVariable SexoEnum sexo) {
		
		List<Cliente> list = service.findBySexo(sexo);
		return ResponseEntity.ok().body(list);

	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Cliente obj){
		
		obj = service.insert(obj);
		//URI de retorno, ao ser criado com sucesso esta disponivel no header
		//o endereço do novo cliente
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(obj.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody Cliente obj, @PathVariable Integer id){
		
		obj.setId(id);
		Cliente bdObj = service.find(obj.getId());
		
		//caso os campos não sejam informados, permanecem os campos que já estão no banco de dados 
		//representado aqui pelo bdObj. Caso o ID requisitado para fazer a atualização não exista
		//já apresentaria ObjectNotFoundException
		if(obj.getNome() == null) obj.setNome(bdObj.getNome());
		if(obj.getIdade() == null) obj.setIdade(bdObj.getIdade());
		if(obj.getCpf() == null) obj.setCpf(bdObj.getCpf());
		if(obj.getSexo() == null) obj.setSexo(bdObj.getSexo());

		obj = service.update(obj);
		
		return ResponseEntity.noContent().build();
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Cliente> delete(@PathVariable Integer id) {
		
		service.delete(id);
		return ResponseEntity.noContent().build();
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Cliente>> findAll() {
		
		List<Cliente> list = service.findAll();
		return ResponseEntity.ok().body(list);
		
	}
	

	//retorna por padrao 24 linhas por pagina por ser multiplo de 1,2,3,4... 
	//facilita organização de layout na tela de forma responsiva
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ResponseEntity<Page> findPage(
			
		  @RequestParam(value = "page",defaultValue = "0") 
		  @Min(value = 0, message = "page valor mínimo: 0")	Integer page,
		  @RequestParam(value = "linesPerPage",defaultValue = "24") 
		  @Max(value=100, message = "linesPerPage valor máximo: 100")
		  @Min(value=1, message = "linesPerPage valor mínimo: 1 ")Integer linesPerPage,
		  @RequestParam(value = "orderBy",defaultValue = "nome")String orderBy,
		  @RequestParam(value = "direction",defaultValue = "ASC")String direction) {
		
		Direction sort = Sort.Direction.ASC;
		
		if(direction.toUpperCase().equals("DESC")){
			sort = Sort.Direction.DESC;
		}
		
		Page<Cliente> list = service.findPage(page, linesPerPage, orderBy,sort);

		return ResponseEntity.ok().body(list);
		
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/pageNome", method = RequestMethod.GET)
	public ResponseEntity<Page> findPageNome(
		  @RequestParam(value = "nome",defaultValue = "")String nome,
		  @RequestParam(value = "page",defaultValue = "0") 
		  @Min(value = 0, message = "page valor mínimo: 0")	Integer page,
		  @RequestParam(value = "linesPerPage",defaultValue = "24") 
		  @Max(value=100, message = "linesPerPage valor máximo: 100")
		  @Min(value=1, message = "linesPerPage valor mínimo: 1 ")Integer linesPerPage,
		  @RequestParam(value = "orderBy",defaultValue = "nome")String orderBy,
		  @RequestParam(value = "direction",defaultValue = "ASC")String direction
		  ) {
		
		Direction sort = Sort.Direction.ASC;
		
		if(direction.toUpperCase().equals("DESC")){
			sort = Sort.Direction.DESC;
		}
		
		Page<Cliente> list = service.findPageNome(page, linesPerPage, orderBy,sort,nome);

		return ResponseEntity.ok().body(list);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/pageIdade", method = RequestMethod.GET)
	public ResponseEntity<Page> findPageIdade(
		  @RequestParam(value = "idade",defaultValue = "")Integer idade,
		  @RequestParam(value = "page",defaultValue = "0") 
		  @Min(value = 0, message = "page valor mínimo: 0")	Integer page,
		  @RequestParam(value = "linesPerPage",defaultValue = "24") 
		  @Max(value=100, message = "linesPerPage valor máximo: 100")
		  @Min(value=1, message = "linesPerPage valor mínimo: 1 ")Integer linesPerPage,
		  @RequestParam(value = "orderBy",defaultValue = "nome")String orderBy,
		  @RequestParam(value = "direction",defaultValue = "ASC")String direction
		  ) {
		
		Direction sort = Sort.Direction.ASC;
		
		if(direction.toUpperCase().equals("DESC")){
			sort = Sort.Direction.DESC;
		}
		
		Page<Cliente> list = service.findPageIdade(page, linesPerPage, orderBy,sort,idade);

		return ResponseEntity.ok().body(list);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/pageSexo", method = RequestMethod.GET)
	public ResponseEntity<Page> findPageSexo(
		  @RequestParam(value = "sexo",defaultValue = "") SexoEnum sexo,
		  @RequestParam(value = "page",defaultValue = "0") 
		  @Min(value = 0, message = "page valor mínimo: 0")	Integer page,
		  @RequestParam(value = "linesPerPage",defaultValue = "24") 
		  @Max(value=100, message = "linesPerPage valor máximo: 100")
		  @Min(value=1, message = "linesPerPage valor mínimo: 1 ")Integer linesPerPage,
		  @RequestParam(value = "orderBy",defaultValue = "nome")String orderBy,
		  @RequestParam(value = "direction",defaultValue = "ASC")String direction
		  ) {
		
		Direction sort = Sort.Direction.ASC;
		
		if(direction.toUpperCase().equals("DESC")){
			sort = Sort.Direction.DESC;
		}
		
		Page<Cliente> list = service.findPageSexo(page, linesPerPage, orderBy,sort,sexo);

		return ResponseEntity.ok().body(list);
	}
}
