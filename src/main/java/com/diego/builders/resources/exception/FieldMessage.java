package com.diego.builders.resources.exception;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class FieldMessage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String fieldName;
	private String messagem;


	public FieldMessage(String fieldName, String messagem) {
		super();
		this.fieldName = fieldName;
		this.messagem = messagem;
	}

}
