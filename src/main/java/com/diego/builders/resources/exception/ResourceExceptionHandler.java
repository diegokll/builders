package com.diego.builders.resources.exception;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.diego.services.exceptions.ObjectNotFoundException;

@ControllerAdvice
public class ResourceExceptionHandler {
	
	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<StandardError> objectNotFound(ObjectNotFoundException e, HttpServletRequest request){
		
		StandardError err = new StandardError(HttpStatus.NOT_FOUND.value(),e.getMessage(), System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
		
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<StandardError> validation(MethodArgumentNotValidException e, HttpServletRequest request){
		
		
		ValidationError err = new ValidationError(HttpStatus.BAD_REQUEST.value(),"Erro de Validação", System.currentTimeMillis());
		
		for(FieldError x : e.getBindingResult().getFieldErrors()){
			err.addError(x.getField(), x.getDefaultMessage());
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
		
	}
		  
	  @ExceptionHandler(ConstraintViolationException.class)
	  ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
	    return new ResponseEntity<>("Erro de validação: " + e.getMessage(), HttpStatus.BAD_REQUEST);
	  }
	  
	  @ExceptionHandler(PropertyReferenceException.class)
	  ResponseEntity<String> handlePropertyReferenceException(PropertyReferenceException e) {
	    return new ResponseEntity<>("Propriedade \""+e.getPropertyName()+"\" não encontrada para busca", HttpStatus.BAD_REQUEST);
	  }
	  
	  
	  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
	  ResponseEntity<String> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
	    return new ResponseEntity<>("Erro de validação, argumento " + e.getName() + 
	    		" Não suporta o valor: \""+e.getValue()+"\"", HttpStatus.BAD_REQUEST);
	  }
	  
	  @ExceptionHandler(HttpMessageNotReadableException.class)
	  ResponseEntity<String> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
	    return new ResponseEntity<>("Erro de validação, argumento " + e.getMessage(), HttpStatus.BAD_REQUEST);
	  }
	  
	  
	  
	
}
