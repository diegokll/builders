package com.diego.builders.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.diego.builders.domain.Cliente;
import com.diego.builders.domain.enums.SexoEnum;
import com.diego.builders.repositories.ClienteRepository;
import com.diego.services.exceptions.ObjectNotFoundException;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository repo;

	public Cliente find(Integer id) {
		Optional<Cliente> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName()));
	}

	public Cliente findByCpf(String cpf) {
		Optional<Cliente> obj = repo.findByCpf(cpf);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Cpf: " + cpf + ", Tipo: " + Cliente.class.getName()));
	}

	public Cliente findByNome(String Nome) {
		Optional<Cliente> obj = repo.findByNome(Nome);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Nome: " + Nome + ", Tipo: " + Cliente.class.getName()));
	}

	public List<Cliente> findBySexo(SexoEnum sexo) {
		return repo.findBySexo(sexo);
	}

	public Cliente insert(Cliente obj) {
		obj.setId(null);
		return repo.save(obj);
	}

	public Cliente update(Cliente obj) {
		return repo.save(obj);
	}

	public void delete(Integer id) {
		// caso o ID requisitado para fazer a atualização não exista apresenta
		// ObjectNotFoundException
		find(id);
		repo.deleteById(id);
	}

	// busca todos os clientes sem paginação ou qualquer tipo de filtro
	public List<Cliente> findAll() {
		return repo.findAll();
	}

	// buscar com configuração de paginas parametrizado
	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, Direction direction) {

		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.by(direction, orderBy));

		return repo.findAll(pageRequest);

	}

	// buscar por fragmento do nome com configuração de paginas parametrizado
	public Page<Cliente> findPageNome(Integer page, Integer linesPerPage, String orderBy, Direction direction,String nome) {

		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.by(direction, orderBy));

		return repo.findPageName(nome,pageRequest);

	}
	
	// buscar por idade com configuração de paginas parametrizado
	public Page<Cliente> findPageIdade(Integer page, Integer linesPerPage, String orderBy, Direction direction,Integer idade) {

		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.by(direction, orderBy));

		return repo.findPageIdade(idade,pageRequest);

	}
	
	// buscar por idade com configuração de paginas parametrizado
	public Page<Cliente> findPageSexo(Integer page, Integer linesPerPage, String orderBy, Direction direction,SexoEnum sexo) {

		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.by(direction, orderBy));

		return repo.findPageSexo(sexo,pageRequest);

	}
}
