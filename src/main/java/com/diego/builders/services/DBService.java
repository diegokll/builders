package com.diego.builders.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diego.builders.domain.Cliente;
import com.diego.builders.domain.enums.SexoEnum;
import com.diego.builders.repositories.ClienteRepository;

@Service
public class DBService {

	@Autowired
	ClienteRepository clienteRepository;

	
	public void instantiateTestDataBase() {
		
		Cliente cliente1 = new Cliente(null,"Diego Henrique",30,"03891340583",SexoEnum.MASCULINO);
		Cliente cliente2 = new Cliente(null,"Ingrid Melo",28,"66219641027",SexoEnum.FEMININO);
		Cliente cliente3 = new Cliente(null,"João Araujo",30,"62885001003",SexoEnum.MASCULINO);
		Cliente cliente4 = new Cliente(null,"Maria José",42,"22048780067",SexoEnum.FEMININO);
		Cliente cliente5 = new Cliente(null,"José",30,"71115262017",SexoEnum.MASCULINO);
		Cliente cliente6 = new Cliente(null,"Luzia Dantas",55,"56470434029",SexoEnum.FEMININO);
		Cliente cliente7 = new Cliente(null,"Carlos Henrique",68,"61191188027",SexoEnum.MASCULINO);
		Cliente cliente8 = new Cliente(null,"Carol Marques",25,"73632191093",SexoEnum.FEMININO);
		Cliente cliente9 = new Cliente(null,"Elisson",33,"14774257052",SexoEnum.MASCULINO);
		Cliente cliente10 = new Cliente(null,"Wender",30,"56550168007",SexoEnum.MASCULINO);

		clienteRepository.saveAll(Arrays.asList(cliente1,cliente2,cliente3,
				cliente4,cliente5,cliente6,cliente7,cliente8,cliente9,cliente10));
		
		
	}
	
}
