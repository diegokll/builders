package com.diego.builders.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.diego.builders.domain.Cliente;
import com.diego.builders.resources.exception.FieldMessage;
import com.diego.builders.services.validation.utils.CpfValidator;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, Cliente> {
	
	@Override
	public void initialize(ClienteInsert ann) {
	}

	@Override
	public boolean isValid(Cliente obj, ConstraintValidatorContext context) {
		
		List<FieldMessage> list = new ArrayList<>();

		// incluindo os testes aqui e inserindo erros na lista
		
		if(!CpfValidator.isValidCpf(obj.getCpf())) {
			list.add(new FieldMessage("Cpf","Cpf Inválido."));
		}
		
		//cada erro adicionado anteriormente na lista, é adicionado na lista de erros do framework
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessagem()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}