package com.diego.builders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.diego.builders.domain.Cliente;
import com.diego.builders.domain.enums.SexoEnum;
import com.diego.builders.services.validation.utils.RestResponsePage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@EnableWebMvc

class BuildersApplicationTests {

	@Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

	
	@Test
	@Order(1)
	void insertTest() throws JsonProcessingException, Exception {
		
		Cliente clienteInsert = new Cliente(null,"Teste Insert",99,"99700981010",SexoEnum.MASCULINO);
		
		mockMvc.perform(post("/api/v1/clientes")
		        .contentType("application/json")
		        .content(objectMapper.writeValueAsString(clienteInsert)))
		        .andExpect(status().isCreated());
		
		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/{id}",11)).andExpect(status().isOk()).andReturn();
		
		Assertions.assertEquals(clienteReturn.getResponse().getContentAsString(),
				"{\"id\":11,\"nome\":\"Teste Insert\",\"idade\":99,\"cpf\":\"99700981010\",\"sexo\":\"MASCULINO\"}");
		
	}
	
	@Test
	@Order(2)
	void updateTest() throws JsonProcessingException, Exception {

		Cliente clienteUpdate = new Cliente();
		
		clienteUpdate.setNome("Teste Update");
		clienteUpdate.setCpf("60221954031");
		clienteUpdate.setIdade(99);
		clienteUpdate.setSexo(SexoEnum.FEMININO);
		
		mockMvc.perform(put("/api/v1/clientes/{id}",10)
		        .contentType("application/json")
		        .content(objectMapper.writeValueAsString(clienteUpdate)))
		        .andExpect(status().isNoContent());
	}

	@Test
	@Order(3)
	void findByIdTestAfterUpdate() throws JsonProcessingException, Exception {
		
		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/{id}",10)).andExpect(status().isOk()).andReturn();
		
		Assertions.assertEquals(clienteReturn.getResponse().getContentAsString(),
				"{\"id\":10,\"nome\":\"Teste Update\",\"idade\":99,\"cpf\":\"60221954031\",\"sexo\":\"FEMININO\"}");

	}
	
	@Test
	@Order(4)
	void deleteById() throws JsonProcessingException, Exception {
		
		mockMvc.perform(delete("/api/v1/clientes/10")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
		
		mockMvc.perform(get("/api/v1/clientes/{id}",10)).andExpect(status().isNotFound());
				
	}
	
	@Test
	@Order(5)
	void findByIdNotFound() throws JsonProcessingException, Exception {
		
		mockMvc.perform(get("/api/v1/clientes/{id}",99)).andExpect(status().isNotFound());
		
		
	}
	
	@Test
	@Order(6)
	void findByCpf() throws JsonProcessingException, Exception {
		
		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/cpf/{cpf}","60221954031"))
				.andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		Cliente response = objectMapper.readValue(contentAsString, Cliente.class);
		
		Assertions.assertEquals(response.getId(),10);
		Assertions.assertEquals(response.getNome(),"Teste Update");
		Assertions.assertEquals(response.getIdade(),99);
		Assertions.assertEquals(response.getCpf(),"60221954031");
		Assertions.assertEquals(response.getSexo(), SexoEnum.FEMININO);

	}
	
	@Test
	@Order(6)
	void findByNome() throws JsonProcessingException, Exception {
		
		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/nome/{nome}","Diego Henrique"))
				.andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		Cliente response = objectMapper.readValue(contentAsString, Cliente.class);
		
		Assertions.assertEquals(response.getId(),1);
		Assertions.assertEquals(response.getNome(),"Diego Henrique");
		Assertions.assertEquals(response.getIdade(),30);
		Assertions.assertEquals(response.getCpf(),"03891340583");
		Assertions.assertEquals(response.getSexo(), SexoEnum.MASCULINO);

	}
	
	@Test
	@Order(7)
	void findBySexo() throws JsonProcessingException, Exception {
		
		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes//sexo/{sexo}","MASCULINO"))
				.andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		Cliente[] response = objectMapper.readValue(contentAsString, Cliente[].class);
		
		Assertions.assertEquals(response.length,6);
		
		Assertions.assertEquals(clienteReturn.getResponse().getContentAsString(),
				"[{\"id\":1,\"nome\":\"Diego Henrique\",\"idade\":30,\"cpf\":\"03891340583\",\"sexo\":\"MASCULINO\"}"
				+ ",{\"id\":3,\"nome\":\"JoÃ£o Araujo\",\"idade\":30,\"cpf\":\"62885001003\",\"sexo\":\"MASCULINO\"}"
				+ ",{\"id\":5,\"nome\":\"JosÃ©\",\"idade\":30,\"cpf\":\"71115262017\",\"sexo\":\"MASCULINO\"}"
				+ ",{\"id\":7,\"nome\":\"Carlos Henrique\",\"idade\":68,\"cpf\":\"61191188027\",\"sexo\":\"MASCULINO\"}"
				+ ",{\"id\":9,\"nome\":\"Elisson\",\"idade\":33,\"cpf\":\"14774257052\",\"sexo\":\"MASCULINO\"}"
				+ ",{\"id\":10,\"nome\":\"Wender\",\"idade\":30,\"cpf\":\"56550168007\",\"sexo\":\"MASCULINO\"}]");

	}
	
	@Test
	@Order(8)
	void findByCpfNotFound() throws JsonProcessingException, Exception {
		
		mockMvc.perform(get("/api/v1/clientes/cpf/{cpf}","99999999")).andExpect(status().isNotFound());
		
		
	}
	
	@Test
	@Order(9)
	void findByNomeNotFound() throws JsonProcessingException, Exception {
		
		mockMvc.perform(get("/api/v1/clientes/nome/{nome}","não existe")).andExpect(status().isNotFound());
		
		
	}
	
	@Test
	@Order(10)
	void findBySexoNotFound() throws JsonProcessingException, Exception {
		
		mockMvc.perform(get("/api/v1/clientes/sexo/{sexo}","Bad-Request")).andExpect(status().isBadRequest());
		
		
	}
	
	@Test
	@Order(11)
	void findAllPaginado() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/page")).andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		RestResponsePage<?> response = objectMapper.readValue(contentAsString,RestResponsePage.class);

		Assertions.assertEquals(response.getContent().size(),10);	
		Assertions.assertEquals(response.getPageable().getPageNumber(),0);
		Assertions.assertEquals(response.getPageable().getPageSize(),24);
		Assertions.assertEquals(response.getPageable().getSort(),Sort.unsorted());
		
		
	}
	
	@Test
	@Order(12)
	void findNomePaginado() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/pageNome?nome={nome}","Henrique"))
				.andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		RestResponsePage<?> response = objectMapper.readValue(contentAsString,RestResponsePage.class);

		Assertions.assertEquals(response.getContent().size(),2);	
		Assertions.assertEquals(response.getPageable().getPageNumber(),0);
		Assertions.assertEquals(response.getPageable().getPageSize(),24);
		Assertions.assertEquals(response.getPageable().getSort(),Sort.unsorted());
		
		
	}
	
	@Test
	@Order(13)
	void findIdadePaginado() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/pageIdade?idade={idade}",30)).andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		RestResponsePage<?> response = objectMapper.readValue(contentAsString,RestResponsePage.class);

		Assertions.assertEquals(response.getContent().size(),3);	
		Assertions.assertEquals(response.getPageable().getPageNumber(),0);
		Assertions.assertEquals(response.getPageable().getPageSize(),24);
		Assertions.assertEquals(response.getPageable().getSort(),Sort.unsorted());
		
		
	}
	
	@Test
	@Order(14)
	void findSexoPaginado() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/pageSexo?sexo={sexo}","MASCULINO")).andExpect(status().isOk()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		RestResponsePage<?> response = objectMapper.readValue(contentAsString,RestResponsePage.class);

		Assertions.assertEquals(response.getContent().size(),5);	
		Assertions.assertEquals(response.getPageable().getPageNumber(),0);
		Assertions.assertEquals(response.getPageable().getPageSize(),24);
		Assertions.assertEquals(response.getPageable().getSort(),Sort.unsorted());
		
		clienteReturn = mockMvc.perform(get("/api/v1/clientes/pageSexo?sexo={sexo}","FEMININO")).andExpect(status().isOk()).andReturn();
		
		contentAsString = clienteReturn.getResponse().getContentAsString();

		response = objectMapper.readValue(contentAsString,RestResponsePage.class);
		
		Assertions.assertEquals(response.getContent().size(),5);	
		Assertions.assertEquals(response.getPageable().getPageNumber(),0);
		Assertions.assertEquals(response.getPageable().getPageSize(),24);
		Assertions.assertEquals(response.getPageable().getSort(),Sort.unsorted());
		
		
	}
	
	@Test
	@Order(15)
	void findAllPaginadoBadRequestLines() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/page?linesPerPage={linesPerPage}",101))
				.andExpect(status().isBadRequest()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();
		
		Assertions.assertEquals(contentAsString,"Erro de validação: findPage.linesPerPage: linesPerPage valor máximo: 100");
	}
	
	@Test
	@Order(16)
	void findAllPaginadoBadRequestArgument() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/page?linesPerPage=1&page=0&orderBy={orderBy}","teste"))
				.andExpect(status().isBadRequest()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();
		
		Assertions.assertEquals(contentAsString,"Propriedade \"teste\" não encontrada para busca");
		
	}
	
	@Test
	@Order(17)
	void findSexoPaginadoBadRequestArgument() throws JsonProcessingException, Exception {

		MvcResult clienteReturn = mockMvc.perform(get("/api/v1/clientes/pageSexo?sexo={sexo}","teste"))
				.andExpect(status().isBadRequest()).andReturn();
		
		String contentAsString = clienteReturn.getResponse().getContentAsString();

		Assertions.assertEquals(contentAsString,"Erro de validação, argumento sexo Não suporta o valor: \"teste\"");
		
	}

}
